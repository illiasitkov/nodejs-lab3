const errorMessage = (message) => ({message});


const errorHandler = (err, req, res, next) => {
  if (err.clientError) {
    res.status(400);
    res.json(errorMessage(err.message));
  } else {
    res.status(500);
    res.json(errorMessage('Server error'));
  }
  next(err);
};

module.exports = errorHandler;


