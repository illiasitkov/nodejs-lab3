const express = require('express');
const asyncHandler = require('./asyncHandler');
const LoadService = require('../services/LoadService');
const {dbLoadsToLoadsGetDto,
  dbLoadToLoadGetDto, dbTruckToTruckGetDto} = require('../mappers/mappers');
const ValidationService = require('../services/ValidationService');
const UserRole = require('../utils/UserRole');

const loadsRouter = new express.Router();

loadsRouter.route('/')
    .get(asyncHandler(async (req, res) => {
      const id = req.decodedUser.userId;
      const role = req.decodedUser.role;
      let limit = req.query.limit;
      limit = limit ? (limit < 0 ? 0 : limit) : 0;
      let offset = req.query.offset;
      offset = offset ? (offset < 0 ? 0 : offset) : 0;
      const status = req.query.status;
      const loads = await LoadService.getLoads(id, role, limit, offset, status);
      res.status(200);
      res.json({loads: dbLoadsToLoadsGetDto(loads)});
    }))
    .post(asyncHandler(async (req, res) => {
      ValidationService.userHasRole(req.decodedUser, UserRole.SHIPPER);
      const id = req.decodedUser.userId;
      await LoadService.createLoad(id, req.body);
      res.status(200);
      res.json({message: 'Load created successfully'});
    }));


loadsRouter.route('/active')
    .get(asyncHandler(async (req, res) => {
      ValidationService.userHasRole(req.decodedUser, UserRole.DRIVER);
      const id = req.decodedUser.userId;
      const load = await LoadService.getActiveLoad(id);
      res.status(200);
      res.json({load: dbLoadToLoadGetDto(load)});
    }));


loadsRouter.route('/active/state')
    .patch(asyncHandler(async (req, res) => {
      ValidationService.userHasRole(req.decodedUser, UserRole.DRIVER);
      const id = req.decodedUser.userId;
      const updatedState = await LoadService.updateActiveLoadState(id);
      res.status(200);
      res.json({message: `Load state changed to \'${updatedState}\'`});
    }));


loadsRouter.route('/:loadId')
    .get(asyncHandler(async (req, res) => {
      const id = req.decodedUser.userId;
      const load = await LoadService.getUserLoadById(id,
          req.decodedUser.role,
          req.params.loadId);
      res.status(200);
      res.json({load: dbLoadToLoadGetDto(load)});
    }))
    .put(asyncHandler(async (req, res) => {
      ValidationService.userHasRole(req.decodedUser, UserRole.SHIPPER);
      const id = req.decodedUser.userId;
      await LoadService.updateUserLoadById(id, req.params.loadId, req.body);
      res.status(200);
      res.json({message: 'Load details changed successfully'});
    }))
    .delete(asyncHandler(async (req, res) => {
      ValidationService.userHasRole(req.decodedUser, UserRole.SHIPPER);
      const id = req.decodedUser.userId;
      await LoadService.deleteUserLoadById(id, req.params.loadId);
      res.status(200);
      res.json({message: 'Load deleted successfully'});
    }));


loadsRouter.route('/:loadId/post')
    .post(asyncHandler(async (req, res) => {
      ValidationService.userHasRole(req.decodedUser, UserRole.SHIPPER);
      const id = req.decodedUser.userId;
      const driverFound = await LoadService.postUserLoad(id, req.params.loadId);
      res.status(200);
      let message;
      if (driverFound) {
        message = 'Load posted successfully';
      } else {
        message = 'Load not posted';
      }
      res.json({message, driver_found: driverFound});
    }));

loadsRouter.route('/:loadId/shipping_info')
    .get(asyncHandler(async (req, res) => {
      ValidationService.userHasRole(req.decodedUser, UserRole.SHIPPER);
      console.log(req.decodedUser);
      const id = req.decodedUser.userId;
      const {load, truck} =
          await LoadService.getShipmentInfoForActiveLoad(id, req.params.loadId);
      res.status(200);
      res.json({load: dbLoadToLoadGetDto(load),
        truck: dbTruckToTruckGetDto(truck)});
    }));

module.exports = loadsRouter;
