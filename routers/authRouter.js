const express = require('express');

const AuthService = require('../services/AuthService');
const asyncHandler = require('./asyncHandler');
const {getUserRoleFromRequest} = require('./authMiddleware');

const authRouter = new express.Router();

authRouter.route('/register')
    .post(asyncHandler(async (req, res) => {
      await AuthService.registerUser(req.body);
      res.status(200);
      res.json({message: 'Success'});
    }));


authRouter.route('/login')
    .post(asyncHandler(async (req, res) => {
      console.log('here');
      const token = await AuthService.loginUser(req.body);
      res.status(200);
      res.json({jwt_token: token});
    }));

authRouter.route('/forgot_password')
    .post(asyncHandler(async (req, res) => {
      await AuthService.resetPassword(req.body);
      res.status(200);
      res.json({message: 'New password sent to your email address'});
    }));

authRouter.route('/role')
    .get(asyncHandler(async (req, res) => {
      const role = await getUserRoleFromRequest(req);
      res.status(200);
      res.json({role});
    }));

module.exports = authRouter;

