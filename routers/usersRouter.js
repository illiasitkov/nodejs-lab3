
const express = require('express');

const userRouter = new express.Router();

const asyncHandler = require('./asyncHandler');
const UserService = require('../services/UserService');
const {dbUserToUserGetDto} = require('../mappers/mappers');
const UserRole = require('../utils/UserRole');
const ValidationService = require('../services/ValidationService');

userRouter.route('/')
    .get(asyncHandler(async (req, res) => {
      const id = req.decodedUser.userId;
      const user = await UserService.findUserById(id);
      res.status(200);
      res.json({user: dbUserToUserGetDto(user)});
    }))
    .delete(asyncHandler(async (req, res) => {
      ValidationService.userHasRole(req.decodedUser, UserRole.SHIPPER);
      const id = req.decodedUser.userId;
      await UserService.deleteUserById(id);
      res.status(200);
      res.json({message: 'Success'});
    }));


userRouter.route('/password')
    .patch(asyncHandler(async (req, res) => {
      const id = req.decodedUser.userId;
      await UserService.changePassword(id, req.body);
      res.status(200);
      res.json({message: 'Success'});
    }));

module.exports = userRouter;


