const jwt = require('jsonwebtoken');

const asyncHandler = require('./asyncHandler');
const UserRole = require('../utils/UserRole');


const verifyTokenWithRole = (role) => async (req, res, next) => {
  const header = req.headers['authorization'];
  if (!header) {
    // eslint-disable-next-line no-throw-literal
    throw {clientError: true, message: 'Access forbidden'};
  }
  const bearer = header.split(/\s+/igm);
  const token = bearer[1];
  try {
    req.decodedUser = await jwt.verify(token, process.env.JWT_SECRET);
    if (role === UserRole.ANY || role === req.decodedUser.role) {
      next();
    } else {
      // eslint-disable-next-line no-throw-literal
      throw {};
    }
  } catch (e) {
    // eslint-disable-next-line no-throw-literal
    throw {
      clientError: true,
      message: 'Authorization failed. Access forbidden'};
  }
};

const getUserRoleFromRequest = async (req) => {
  const header = req.headers['authorization'];
  if (!header) {
    // eslint-disable-next-line no-throw-literal
    throw {clientError: true, message: 'Access forbidden'};
  }
  const bearer = header.split(/\s+/igm);
  const token = bearer[1];
  try {
    const decodedUser = await jwt.verify(token, process.env.JWT_SECRET);
    return decodedUser.role;
  } catch (e) {
    // eslint-disable-next-line no-throw-literal
    throw {
      clientError: true,
      message: 'Authorization failed. Access forbidden'};
  }
};

const authMiddlewareWithRole=(role)=>asyncHandler(verifyTokenWithRole(role));

exports.authMiddlewareWithRole = authMiddlewareWithRole;
exports.getUserRoleFromRequest = getUserRoleFromRequest;

