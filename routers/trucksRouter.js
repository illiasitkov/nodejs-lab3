const express = require('express');
const asyncHandler = require('./asyncHandler');
const {dbTrucksToTrucksGetDto,
  dbTruckToTruckGetDto} = require('../mappers/mappers');
const TruckService = require('../services/TruckService');

const trucksRouter = new express.Router();


trucksRouter.route('/')
    .get(asyncHandler(async (req, res) => {
      const id = req.decodedUser.userId;
      const trucks = await TruckService.findAllByUserId(id);
      res.status(200);
      res.json({trucks: dbTrucksToTrucksGetDto(trucks)});
    }))
    .post(asyncHandler(async (req, res) => {
      const id = req.decodedUser.userId;
      await TruckService.createTruck(req.body, id);
      res.status(200);
      res.json({message: 'Truck created successfully'});
    }));

trucksRouter.route('/assigned')
    .get(asyncHandler(async (req, res) => {
      const id = req.decodedUser.userId;
      const truck = await TruckService.getAssignedTruck(id);
      if (!truck) {
        // eslint-disable-next-line no-throw-literal
        throw {
          clientError: true,
          message: 'No assigned truck found',
        };
      }
      res.status(200);
      res.json({truck: dbTruckToTruckGetDto(truck)});
    }));

trucksRouter.route('/:truckId')
    .get(asyncHandler(async (req, res) => {
      const id = req.decodedUser.userId;
      const truck = await TruckService.getUserTruckById(id, req.params.truckId);
      res.status(200);
      res.json({truck: dbTruckToTruckGetDto(truck)});
    }))
    .put(asyncHandler(async (req, res) => {
      const id = req.decodedUser.userId;
      await TruckService.updateTruckById(id, req.params.truckId, req.body);
      res.status(200);
      res.json({message: 'Truck details changed successfully'});
    }))
    .delete(asyncHandler(async (req, res) => {
      const id = req.decodedUser.userId;
      await TruckService.deleteTruckById(id, req.params.truckId);
      res.status(200);
      res.json({message: 'Truck deleted successfully'});
    }));


trucksRouter.route('/:truckId/assign')
    .post(asyncHandler(async (req, res) => {
      const id = req.decodedUser.userId;
      await TruckService.assignTruck(id, req.params.truckId);
      res.status(200);
      res.json({message: 'Truck assign successfully'});
    }));


module.exports = trucksRouter;
