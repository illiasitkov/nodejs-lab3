const nodemailer=require('nodemailer');
const UserService = require('./UserService');

const transporter = nodemailer.createTransport({
  host: 'smtp.gmail.com',
  port: 587,
  secure: false,
  auth: {
    user: process.env.EMAIL_ADDRESS,
    pass: process.env.EMAIL_PASSWORD,
  },
});

const sendEmail = async (email, subject, message) => {
  try {
    await transporter.sendMail({
      from: process.env.EMAIL_ADDRESS,
      to: email,
      subject: subject,
      text: message,
    });
  } catch (e) {
    // eslint-disable-next-line no-throw-literal
    throw {
      clientError: true,
      message: 'Email sending failed. '+e.message};
  }
};

const notifyUser = async (userId, message) => {
  try {
    const user = await UserService.findUserById(userId);
    await sendEmail(user.email, 'Shipment update', message);
  } catch (e) {
    console.log('Notify user exception: ', e);
  }
};

exports.sendEmail = sendEmail;
exports.notifyUser = notifyUser;

