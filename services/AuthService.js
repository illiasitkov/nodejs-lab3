const bcrypt = require('bcrypt');
const UserService = require('../services/UserService');
const ValidationService = require('./ValidationService');
const jwt = require('jsonwebtoken');
const {sendEmail} = require('./EmailService');


const registerUser = async (user) => {
  ValidationService.checkUserPostDto(user);
  if (await ValidationService.userExistsInDB(user)) {
    // eslint-disable-next-line no-throw-literal
    throw {
      clientError: true,
      message: 'User with such an email already exists'};
  }
  const hashedPassword = await bcrypt.hash(user.password, 10);
  return UserService
      .saveUser({email: user.email, password: hashedPassword, role: user.role});
};

const generateToken = (dbUser) => {
  return jwt.sign(
      {userId: dbUser._id, role: dbUser.role},
      process.env.JWT_SECRET,
      {expiresIn: '9000s'});
};

const loginUser = async (user) => {
  ValidationService.checkEmail(user.email);
  ValidationService.checkPassword(user.password);
  if (!await ValidationService.userExistsInDB(user)) {
    // eslint-disable-next-line no-throw-literal
    throw {
      clientError: true,
      message: 'User with such an email does not exists'};
  }
  const dbUser = await UserService.findUserByEmail(user.email);
  if (await bcrypt.compare(user.password, dbUser.password)) {
    return generateToken(dbUser);
  } else {
    // eslint-disable-next-line no-throw-literal
    throw {
      clientError: true,
      message: 'User\'s password is incorrect. Login failed'};
  }
};

const generatePassword = () => {
  return Math.random().toString(36).substring(2, 8);
};

const resetPassword = async (emailObj) => {
  const email = emailObj.email;
  ValidationService.checkEmail(email);
  const dbUser = await UserService.findUserByEmail(email);
  const newPassword = generatePassword();
  await UserService.updatePassword(dbUser._id, newPassword);
  return sendEmail(email,
      'New Password',
      `Your new password is: ${newPassword}`);
};

const AuthService = {
  registerUser, loginUser, resetPassword,
};

module.exports = AuthService;
