
const User = require('../models/User');
const Joi = require('joi');
const UserRole = require('../utils/UserRole');
const TruckType = require('../utils/TruckType');
const Truck = require('../models/Truck');

const passwordSchema = Joi.string().min(4).required();
const emailSchema = Joi.string().email().required();

const userPostSchema = Joi.object({
  email: emailSchema,
  password: passwordSchema,
  role: Joi.string().valid(UserRole.SHIPPER, UserRole.DRIVER).required(),
});


const truckPostSchema = Joi.object({
  type: Joi.string().valid(TruckType.SPRINTER.name,
      TruckType.LARGE_STRAIGHT.name, TruckType.SMALL_STRAIGHT.name).required(),
});

const dimensionsSchema = Joi.object({
  width: Joi.number().min(0).required(),
  length: Joi.number().min(0).required(),
  height: Joi.number().min(0).required(),
});

const loadPostSchema = Joi.object({
  name: Joi.string().trim().min(1).required(),
  payload: Joi.number().min(0).required(),
  pickup_address: Joi.string().trim().min(1).required(),
  delivery_address: Joi.string().trim().min(1).required(),
  dimensions: dimensionsSchema,
});

const checkEmail = (email) => {
  const res = emailSchema.validate(email);
  if (res.error) {
    // eslint-disable-next-line no-throw-literal
    throw {
      clientError: true,
      message: 'Email is invalid or not set',
    };
  }
};

const checkPassword = (password) => {
  const res = passwordSchema.validate(password);
  if (res.error) {
    // eslint-disable-next-line no-throw-literal
    throw {
      clientError: true,
      message: 'Password is invalid or not set',
    };
  }
};


const userExistsInDB = async (user) => {
  const users = await User.find({email: user.email});
  return users.length > 0;
};

const checkUserPostDto = (user) => {
  const res = userPostSchema.validate(user);
  if (res.error) {
    // eslint-disable-next-line no-throw-literal
    throw {
      clientError: true,
      message: 'User is invalid or not set',
    };
  }
};


const userHasRole = (decodedUser, role) => {
  if (decodedUser.role !== role) {
    // eslint-disable-next-line no-throw-literal
    throw {
      clientError: true,
      message: 'User does not have the appropriate role. Access forbidden',
    };
  }
};

const checkTruck = (truckObj) => {
  const res = truckPostSchema.validate(truckObj);
  if (res.error) {
    // eslint-disable-next-line no-throw-literal
    throw {
      clientError: true,
      message: 'Truck is invalid or not set',
    };
  }
};


const checkLoadPostDto = (loadObj) => {
  const res = loadPostSchema.validate(loadObj);
  if (res.error) {
    // eslint-disable-next-line no-throw-literal
    throw {
      clientError: true,
      message: 'Load is invalid or not set',
    };
  }
};

const loadHasStatus = (load, status) => {
  if (load.status !== status) {
    // eslint-disable-next-line no-throw-literal
    throw {
      clientError: true,
      message: 'Load status must be NEW, but it is not',
    };
  }
};

const driverHasNoAssignedTrucks = async (userId) => {
  const trucks = await Truck.find({assigned_to: userId});
  if (trucks.length > 0) {
    // eslint-disable-next-line no-throw-literal
    throw {
      clientError: true,
      message: 'Driver already has a truck assigned to him',
    };
  }
};

const ValidationService = {
  checkUserPostDto, userExistsInDB, checkPassword, userHasRole, checkEmail,
  checkTruck, checkLoadPostDto, loadHasStatus, driverHasNoAssignedTrucks,
};


module.exports = ValidationService;
