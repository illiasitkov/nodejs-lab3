const UserRole = require('../utils/UserRole');
const LoadStatus = require('../utils/LoadStatus');
const Load = require('../models/Load');
const ValidationService = require('./ValidationService');
const LoadState = require('../utils/LoadState');
const TruckService = require('./TruckService');
const TruckStatus = require('../utils/TruckStatus');
const UserService = require('./UserService');
const {notifyUser} = require('./EmailService');


const getLoads = async (userId, role, limit, offset, status) => {
  const filterObj = {};
  let statuses;
  let availableStatuses;
  if (role === UserRole.DRIVER) {
    availableStatuses = [LoadStatus.ASSIGNED, LoadStatus.SHIPPED];
    filterObj.assigned_to = userId;
  } else {
    availableStatuses = [LoadStatus.ASSIGNED, LoadStatus.SHIPPED,
      LoadStatus.NEW, LoadStatus.POSTED];
    filterObj.created_by = userId;
  }

  if (!status) {
    statuses = availableStatuses;
  } else if (!availableStatuses.includes(status)) {
    // eslint-disable-next-line no-throw-literal
    throw {
      clientError: true,
      message: 'The status is unavailable for this user role'};
  } else {
    statuses = [status];
  }

  filterObj.status = {
    $in: statuses,
  };

  return Load.find(filterObj).skip(offset).limit(limit);
};

const createLoad = (userId, loadObj) => {
  ValidationService.checkLoadPostDto(loadObj);
  loadObj.created_by = userId;
  loadObj.assigned_to = '';
  loadObj.status = LoadStatus.NEW;
  loadObj.state = '';
  loadObj.logs = [];
  return Load.create(loadObj);
};

const getActiveLoad = async (userId) => {
  const load = await Load.findOne(
      {
        assigned_to: userId,
        status: LoadStatus.ASSIGNED,
      });
  if (!load) {
    // eslint-disable-next-line no-throw-literal
    throw {
      clientError: true,
      message: 'No active load found'};
  }
  return load;
};


const updateActiveLoadState = async (userId) => {
  const activeLoad = await getActiveLoad(userId);
  const currentState = activeLoad.state;
  if (currentState === LoadState.STATE_3) {
    // eslint-disable-next-line no-throw-literal
    throw {
      clientError: true,
      message: 'State iteration is not possible. State is final already',
    };
  }
  const nextState = LoadState.nextState(currentState);
  activeLoad.state = nextState;

  await Load.create(activeLoad);

  const message = `Load's [id: ${activeLoad._id}] 
  state changed to: ${nextState}`;

  await writeLogToLoad(activeLoad, message);
  notifyUser(userId, message);
  notifyUser(activeLoad.created_by, message);

  if (nextState === LoadState.STATE_3) {
    activeLoad.status = LoadStatus.SHIPPED;
    const message = `Load's [id: ${activeLoad._id}] 
        status changed to: ${LoadStatus.SHIPPED}`;
    await writeLogToLoad(activeLoad, message);
    notifyUser(userId, message);
    notifyUser(activeLoad.created_by, message);

    await TruckService.setUserTruckStatus(userId, TruckStatus.IS);
  }

  return nextState;
};


const getUserLoadById = async (userId, role, loadId) => {
  try {
    let load;
    if (role === UserRole.DRIVER) {
      load = await Load.findOne({assigned_to: userId, _id: loadId});
    } else {
      load = await Load.findOne({created_by: userId, _id: loadId});
    }
    if (!load) {
      // eslint-disable-next-line no-throw-literal
      throw {};
    }
    return load;
  } catch (e) {
    // eslint-disable-next-line no-throw-literal
    throw {
      clientError: true,
      message: 'User does not have a load with such an id',
    };
  }
};

const updateUserLoadById = async (userId, loadId, loadObj) => {
  ValidationService.checkLoadPostDto(loadObj);
  const load = await getUserLoadById(userId, UserRole.SHIPPER, loadId);
  ValidationService.loadHasStatus(load, LoadStatus.NEW);
  load.name = loadObj.name;
  load.payload = loadObj.payload;
  load.pickup_address = loadObj.pickup_address;
  load.delivery_address = loadObj.delivery_address;
  load.dimensions = loadObj.dimensions;
  return Load.create(load);
};

const deleteUserLoadById = async (userId, loadId) => {
  try {
    const load = await getUserLoadById(userId, UserRole.SHIPPER, loadId);
    ValidationService.loadHasStatus(load, LoadStatus.NEW);
    await Load.deleteOne({_id: loadId, created_by: userId});
  } catch (e) {
    // eslint-disable-next-line no-throw-literal
    throw {
      clientError: true,
      message: 'User does not have a load ' +
          'with such an id or the load\'s status is not NEW',
    };
  }
};

const changeLoadStatus = (load, newStatus) => {
  load.status = newStatus;
  return Load.create(load);
};

const changeLoadState = (load, newState) => {
  load.state = newState;
  return Load.create(load);
};

const writeLogToLoad = (load, message) => {
  const log = {
    time: new Date().toISOString(),
    message,
  };
  load.logs.push(log);
  return Load.create(load);
};

const assignLoadToDriver = (load, driver) => {
  load.assigned_to = driver._id;
  return Load.create(load);
};

const postUserLoad = async (userId, loadId) => {
  const load = await getUserLoadById(userId, UserRole.SHIPPER, loadId);
  ValidationService.loadHasStatus(load, LoadStatus.NEW);
  await changeLoadStatus(load, LoadStatus.POSTED);

  const message = `Load's 
  [id: ${load._id}] status changed to POSTED.`;

  await writeLogToLoad(load, message);
  notifyUser(userId, message);
  const truck = await TruckService.getFreeTruckWithDriverForLoad(load);
  if (truck) {
    const driver = await UserService.findUserById(truck.assigned_to);
    await TruckService.changeTruckStatus(truck, TruckStatus.OL);
    await assignLoadToDriver(load, driver);
    await changeLoadStatus(load, LoadStatus.ASSIGNED);
    await changeLoadState(load, LoadState.STATE_0);

    const message = `Load's [id: ${load._id}] status 
    changed to ASSIGNED. Load [id: ${load._id}] 
    assigned to the driver with id: ${driver._id}`;

    await writeLogToLoad(load, message);
    notifyUser(userId, message);
    notifyUser(driver._id, message);
    return true;
  } else {
    await changeLoadStatus(load, LoadStatus.NEW);

    const message = `Driver not found. 
    Load's [id: ${load._id}] status changed back to NEW.`;

    await writeLogToLoad(load, message);
    notifyUser(userId, message);
    return false;
  }
};


const getShipmentInfoForActiveLoad = async (userId, loadId) => {
  try {
    const load = await getUserLoadById(userId, UserRole.SHIPPER, loadId);
    if (load.status !== LoadStatus.ASSIGNED) {
      throw load;
    }
    const truck = await TruckService.getAssignedTruck(load.assigned_to);
    if (!truck) {
      // eslint-disable-next-line no-throw-literal
      throw {
        clientError: true,
        message: 'No truck found',
      };
    }
    return {load, truck};
  } catch (e) {
    // eslint-disable-next-line no-throw-literal
    throw {
      clientError: true,
      message: 'Load status in not ASSIGNED',
    };
  }
};


const LoadService = {
  getLoads, createLoad, getActiveLoad, updateActiveLoadState,
  getUserLoadById, updateUserLoadById, deleteUserLoadById,
  postUserLoad, getShipmentInfoForActiveLoad,
};

module.exports = LoadService;
