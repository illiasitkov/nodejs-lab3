
const User = require('../models/User');
const ValidationService = require('./ValidationService');
const bcrypt = require('bcrypt');

const saveUser = (user) => {
  return User.create(user);
};

const findUserByEmail = async (email) => {
  const user = await User.findOne({email});
  if (user === null) {
    // eslint-disable-next-line no-throw-literal
    throw {clientError: true, message: 'No user with such an email found'};
  }
  return user;
};

const findUserById = async (id) => {
  try {
    const user = await User.findById(id);
    if (user === null) {
      throw user;
    }
    return user;
  } catch (err) {
    // eslint-disable-next-line no-throw-literal
    throw {clientError: true, message: 'No user with such an ID found: '+id};
  }
};

const deleteUserById = async (id) => {
  try {
    await User.deleteOne({_id: id});
  } catch (err) {
    // eslint-disable-next-line no-throw-literal
    throw {clientError: true, message: 'No user with such an ID found'};
  }
};

const changePassword = async (userId, passwords) => {
  const oldPassword = passwords.oldPassword;
  const newPassword = passwords.newPassword;
  ValidationService.checkPassword(oldPassword);
  ValidationService.checkPassword(newPassword);
  const user = await findUserById(userId);
  if (await bcrypt.compare(oldPassword, user.password)) {
    return updatePassword(userId, newPassword);
  } else {
    // eslint-disable-next-line no-throw-literal
    throw {clientError: true, message: 'Old password is incorrect'};
  }
};


const updatePassword = async (userId, newPassword) => {
  const newHashedPassword = await bcrypt.hash(newPassword, 10);
  return User.findByIdAndUpdate(
      {_id: userId},
      {$set: {password: newHashedPassword}},
      {runValidators: true});
};


const UserService = {
  saveUser, findUserByEmail,
  findUserById, deleteUserById, changePassword, updatePassword,
};


module.exports = UserService;


