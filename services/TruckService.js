
const Truck = require('../models/Truck');
const TruckStatus = require('../utils/TruckStatus');
const TruckType = require('../utils/TruckType');
const ValidationService = require('./ValidationService');

const findAllByUserId = (userId) => {
  return Truck.find({created_by: userId});
};

const createTruck = (truckObj, userId) => {
  ValidationService.checkTruck(truckObj);
  const truck = {
    type: truckObj.type,
    created_by: userId,
    assigned_to: '',
    status: TruckStatus.IS,
  };
  return Truck.create(truck);
};

const getUserTruckById = async (userId, truckId) => {
  try {
    const truck = await Truck.findOne({created_by: userId, _id: truckId});
    if (truck === null) {
      throw truck;
    }
    return truck;
  } catch (e) {
    // eslint-disable-next-line no-throw-literal
    throw {
      clientError: true,
      message: 'User\'s truck with such an id is not found'};
  }
};

const updateTruckById = async (userId, truckId, truckObj) => {
  await driverIsNotOnLoad(userId);
  ValidationService.checkTruck(truckObj);
  try {
    if (! await truckIsAssignedTo(truckId, userId)) {
      await Truck.findOneAndUpdate(
          {_id: truckId, created_by: userId},
          {$set: {type: truckObj.type}},
          {runValidators: true});
    } else {
      // eslint-disable-next-line no-throw-literal
      throw {};
    }
  } catch (e) {
    // eslint-disable-next-line no-throw-literal
    throw {
      clientError: true,
      message: 'Truck update failed: the truck is either ' +
          'assigned to the driver or its id is incorrect'};
  }
};

const truckIsAssignedTo = async (truckId, userId) => {
  try {
    const truck = await Truck.findById(truckId);
    return truck.assigned_to === userId;
  } catch (e) {
    // eslint-disable-next-line no-throw-literal
    throw {
      clientError: true,
      message: 'Truck assignment check failed'};
  }
};


const deleteTruckById = async (userId, truckId) => {
  try {
    if (! await truckIsAssignedTo(truckId, userId)) {
      return await Truck.deleteOne(
          {_id: truckId, created_by: userId});
    } else {
      // eslint-disable-next-line no-throw-literal
      throw {};
    }
  } catch (e) {
    // eslint-disable-next-line no-throw-literal
    throw {
      clientError: true,
      message: 'Truck deletion failed'};
  }
};

const assignTruck = async (userId, truckId) => {
  await driverIsNotOnLoad(userId);
  try {
    const assignedTruck = await getAssignedTruck(userId);
    const truck = await Truck.findById(truckId);
    if (!truck || truck.assigned_to) {
      throw truck;
    }
    truck.assigned_to = userId;
    if (assignedTruck) {
      assignedTruck.assigned_to = '';
      await Truck.create(assignedTruck);
    }
    return Truck.create(truck);
  } catch (e) {
    // eslint-disable-next-line no-throw-literal
    throw {
      clientError: true,
      message: 'Truck assignment failed. ' +
          'Truck either does not exist or is assigned to another driver'};
  }
};

const getAssignedTruck = async (userId) => {
  return Truck.findOne({assigned_to: userId});
};

const setUserTruckStatus = async (userId, status) => {
  const assignedTruck = await getAssignedTruck(userId);
  if (!assignedTruck) {
    // eslint-disable-next-line no-throw-literal
    throw {
      clientError: true,
      message: 'Driver does not have an assigned truck',
    };
  }
  assignedTruck.status = status;
  return Truck.create(assignedTruck);
};

const changeTruckStatus = (truck, status) => {
  truck.status = status;
  return Truck.create(truck);
};

const getTruckTypeLoadFits = (load) => {
  const types = [TruckType.SPRINTER,
    TruckType.SMALL_STRAIGHT, TruckType.LARGE_STRAIGHT];
  const type = types.filter((t) => {
    return t.max_weight > load.payload &&
    t.dimensions.width > load.dimensions.width &&
    t.dimensions.length > load.dimensions.length &&
    t.dimensions.height > load.dimensions.height;
  });
  if (type.length === 0) return null;
  return type[0];
};

const getFreeTruckWithDriverForLoad = (load) => {
  const truckType = getTruckTypeLoadFits(load);
  if (!truckType) {
    return null;
  }
  return Truck.findOne({
    status: TruckStatus.IS,
    assigned_to: {$ne: ''},
    type: truckType.name,
  });
};

const driverIsNotOnLoad = async (userId) => {
  try {
    const truck = await getAssignedTruck(userId);
    if (!truck) return;
    if (truck.status === TruckStatus.OL) {
      // eslint-disable-next-line no-throw-literal
      throw {
        incorrectStatus: true,
      };
    }
  } catch (e) {
    if (e.incorrectStatus) {
      // eslint-disable-next-line no-throw-literal
      throw {
        clientError: true,
        message: 'Driver is currently on load',
      };
    }
  }
};

const TruckService = {
  findAllByUserId, createTruck, getUserTruckById, updateTruckById,
  deleteTruckById, assignTruck, setUserTruckStatus, changeTruckStatus,
  getFreeTruckWithDriverForLoad, getAssignedTruck, driverIsNotOnLoad,
};


module.exports = TruckService;


