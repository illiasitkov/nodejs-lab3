# Starting the application

## Setting all the dependencies

In the project root directory, you should run command 
to install dependencies for the server:

### `npm install`

Then change directory to '/client' and run command to
install dependencies for the client:

### `cd /client`
### `npm install`

## Starting the app

To start the server, in the root project
directory you should run:

### `npm start`

To start the client, change directory to '/client' and run the following command:

### `cd /client`
### `npm start`

