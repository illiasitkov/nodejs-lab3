
const TruckType = {
    SPRINTER:  'SPRINTER',
    SMALL_STRAIGHT: 'SMALL STRAIGHT',
    LARGE_STRAIGHT: 'LARGE STRAIGHT',
};

module.exports = TruckType;

