import React, {useEffect, useState} from 'react';
import {useParams} from 'react-router-dom';
import {Button} from 'reactstrap';
import TruckType from "../shared/TruckType";
import {getTruckById, updateTruck} from "../services/TruckService";
import {dateToString} from "../services/Utils";

const TOKEN = 'token';

const TruckPageComponent = ({logout, editable}) => {
  const params = useParams();

  const [truck, setTruck] = useState(null);
  const [type, setType] = useState('');
  const [message, setMessage] = useState(null);

  useEffect(() => {
    loadData();
  }, []);

  useEffect(() => {
    setType(truck?.type ? truck.type: '');
  }, [truck]);

  const token = 'Bearer '+localStorage.getItem(TOKEN);

  const loadData = async () => {
    try {
      const res = await getTruckById(params.truckId, token);
      const obj = await res.json();
      if (res.status === 200) {
        setTruck(obj.truck);
      } else if (obj.message.toLowerCase().includes('forbidden')) {
        alert('Your session is up. Please log in again');
        logout();
      } else {
        alert(obj.message);
      }
    } catch (e) {
      console.log(e);
    }
  };

  const changeHandler = (setValue) => (e) => {
    setMessage(null);
    setValue(e.target.value);
  };

  const update = async (event) => {
    setMessage(null);
    event.preventDefault();
    try {
      const res = await updateTruck(params.truckId, type, token);
      const obj = await res.json();
      if (res.status === 200) {
        loadData();
      } else if (obj.message.toLowerCase().includes('forbidden')) {
        alert('Your session is up. Please log in again');
        logout();
      }
      setMessage(obj.message);
    } catch (e) {
      console.log(e);
    }
  };

  return (
      <div className='container text-start'>
        <div className='row mb-5'>
          <div className='col-md-8 ms-auto me-auto col-12'>
            <div className='container'>
              <div className='row mt-4 mb-3'>
                <h3 className='col-12'>Truck</h3>
              </div>

              {editable &&
                  <div className='row'>
                    <hr/>
                    <h3 className='col-12 mb-3 mt-4'>Edit Truck</h3>
                    <form onSubmit={update}>
                      <div>
                        <label htmlFor='text'>Change truck's type</label>
                        <select
                            value={type}
                            onChange={changeHandler(setType)}
                            id='type'
                            required={true}
                            className='form-control'>
                          <option value={TruckType.SPRINTER}>{TruckType.SPRINTER}</option>
                          <option value={TruckType.SMALL_STRAIGHT}>{TruckType.SMALL_STRAIGHT}</option>
                          <option value={TruckType.LARGE_STRAIGHT}>{TruckType.LARGE_STRAIGHT}</option>
                        </select>
                      </div>
                      <div>
                        <Button type='submit'>Update</Button>
                      </div>
                      {message &&
                          <div className='text-center'>
                            <p>{message}</p>
                          </div>
                      }
                    </form>
                  </div>}

              {truck &&
                  <div className='row'>
                    <hr/>
                    <h5>Created by:</h5>
                    <p>{truck.created_by}</p>
                    <h5>Assigned to:</h5>
                    <p>{truck.assigned_to}</p>
                    <h5>Status:</h5>
                    <p>{truck.status}</p>
                    <h5>Created date:</h5>
                    <p>{dateToString(truck.created_date)}</p>
                    <h5>Type:</h5>
                    <p>{truck.type}</p>
                  </div>}

            </div>
          </div>
        </div>
      </div>
  );
};


export default TruckPageComponent;


