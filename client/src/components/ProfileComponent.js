import React, {useEffect, useState} from 'react';
import {Button} from 'reactstrap';
import {changePassword,
  getUserProfile,
  deleteUser} from '../services/UserService';
import UserRole from "../shared/UserRole";

const TOKEN = 'token';

const ProfileComponent = ({logout, role}) => {
  const [user, setUser] = useState(null);
  const [oldPassword, setOldPassword] = useState('');
  const [newPassword1, setNewPassword1] = useState('');
  const [newPassword2, setNewPassword2] = useState('');
  const [message, setMessage] = useState(null);

  useEffect(() => {
    loadData();
  }, []);

  const changeHandler = (setValue) => (e) => {
    setMessage(null);
    setValue(e.target.value);
  };

  const loadData = async () => {
    try {
      const res = await getUserProfile(token);
      const obj = await res.json();
      if (res.status === 200) {
        setUser(obj.user);
      } else if (obj.message.toLowerCase().includes('forbidden')) {
        alert('Your session is up. Please log in again');
        logout();
      } else {
        alert(obj.message);
      }
    } catch (err) {
      console.log(err);
    }
  };

  const token = 'Bearer '+localStorage.getItem(TOKEN);

  const updatePassword = async (event) => {
    setMessage('');
    event.preventDefault();
    if (newPassword1 !== newPassword2) {
      setMessage('New passwords do not match');
    } else {
      try {
        const res = await changePassword(token, oldPassword, newPassword1);
        const obj = await res.json();
        if (res.status === 200) {
          loadData();
          setOldPassword('');
          setNewPassword1('');
          setNewPassword2('');
        } else if (obj.message.toLowerCase().includes('forbidden')) {
          alert('Your session is up. Please log in again');
          logout();
        }
        setMessage(obj.message);
      } catch (err) {
        console.log(err);
      }
    }
  };


  const delUser = async () => {
    try {
      const res = await deleteUser(token);
      const obj = await res.json();
      if (res.status === 200) {
        logout();
      } else if (obj.message.toLowerCase().includes('forbidden')) {
        alert('Your session is up. Please log in again');
        logout();
      } else {
        setMessage(obj.message);
      }
    } catch (err) {
      console.log(err);
    }
  };


  return (
    <div className='container text-start'>
      <div className='row mt-5'>
        <form
          className='col-md-6 col-lg-4 col-8 ms-auto me-auto'
          onSubmit={updatePassword}>
          <div>
            <h3 className='text-center'>Profile</h3>
          </div>
          {message &&
                        <div className='text-center'>
                          <p>{message}</p>
                        </div>}
          <div>
            <label htmlFor='email'>Email</label>
            <input
              disabled={true}
              value={user?.email}
              type='text' id='email' className='form-control'/>
          </div>
          <div>
            <label htmlFor='role'>Role</label>
            <input
                disabled={true}
                value={user?.role}
                type='text' id='role' className='form-control'/>
          </div>
          <div>
            <label htmlFor='password'>Old password</label>
            <input
              value={oldPassword}
              onChange={changeHandler(setOldPassword)}
              type='password' id='password'
              required={true} className='form-control'/>
          </div>
          <div>
            <label htmlFor='npassword1'>New password</label>
            <input
              value={newPassword1}
              onChange={changeHandler(setNewPassword1)}
              type='password' id='npassword1'
              required={true} className='form-control'/>
          </div>
          <div>
            <label htmlFor='npassword2'>Repeat new password</label>
            <input
              value={newPassword2}
              onChange={changeHandler(setNewPassword2)}
              type='password' id='npassword2'
              required={true} className='form-control'/>
          </div>
          <div className='d-flex gap-3'>
            <button
              className='btn btn-primary'
              type='submit'>Change password</button>
            {role === UserRole.SHIPPER &&
                <Button
                  type='button'
                  className='btn-danger' onClick={delUser}>Delete account
                </Button>}
          </div>
        </form>
      </div>
    </div>
  );
};


export default ProfileComponent;


