import React, {useEffect, useState} from 'react';
import {Button} from 'reactstrap';
import {useNavigate} from 'react-router-dom';
import TruckType from "../shared/TruckType";
import {assignTruck, deleteTruck, loadActTruck, loadTrucks, saveTruck} from "../services/TruckService";
import TruckStatus from "../shared/TruckStatus";

const TOKEN = 'token';

const TruckComponent = ({truck, reloadData, token, logout, assignedTruck}) => {

  const navigate = useNavigate();

  const editable = truck._id !== assignedTruck?._id;

  const driverIsOnLoad = assignedTruck && assignedTruck.status === TruckStatus.OL;

  const canBeAssigned = !driverIsOnLoad && !truck.assigned_to;

  const assign = async () => {
    try {
      const res = await assignTruck(truck._id, token);
      const obj = await res.json();
      if (res.status === 200) {
        reloadData();
      } else if (obj.message.toLowerCase().includes('forbidden')) {
        alert('Your session is up. Please log in again');
        logout();
      } else {
        alert(obj.message);
      }
    } catch (e) {
      console.log(e);
    }
  };

  const delTruck = async () => {
    try {
      const res = await deleteTruck(truck._id, token);
      const obj = await res.json();
      if (res.status === 200) {
        reloadData();
      } else if (obj.message.toLowerCase().includes('forbidden')) {
        alert('Your session is up. Please log in again');
        logout();
      } else {
        alert(obj.message);
      }
    } catch (e) {
      console.log(e);
    }
  };

  const goToTruckPage = () => {
    let path;
    if (editable) {
      path = `${truck._id}/edit`;
    } else {
      path = `${truck._id}/details`
    }
    navigate(path, {replace: false});
  };

  return (
    <div
      className='note-card flex-wrap flex-md-row flex-column gap-2
        d-flex justify-content-between align-items-center'>
      <div>
        {truck.type} | {truck._id}
      </div>
      <div className='d-flex gap-3 flex-wrap justify-content-center'>
        <Button onClick={goToTruckPage}>{editable ? 'Edit' : 'Details'}</Button>

        {canBeAssigned &&
            <Button onClick={assign} className='btn-info'>Assign</Button>}

        {editable &&
            <Button onClick={delTruck} className='btn-danger'>Delete</Button>}

      </div>
    </div>
  );
};


const TruckListComponent = ({logout}) => {
  const [trucks, setTrucks] = useState([]);
  const [activeTruck, setActiveTruck] = useState(null);
  const [type, setType] = useState(TruckType.SPRINTER);

  const token = 'Bearer '+localStorage.getItem(TOKEN);

  useEffect(() => {
    loadAllData();
  }, []);

  const changeHandler = (setValue) => (e) => {
    setValue(e.target.value);
  };

  const loadAllData = () => {
    loadData();
    loadActiveTruck();
  }

  const loadData = async () => {
    try {
      const res = await loadTrucks(token);
      const obj = await res.json();
      if (res.status === 200) {
        setTrucks(obj.trucks);
      } else if (obj.message.toLowerCase().includes('forbidden')) {
        alert('Your session is up. Please log in again');
        logout();
      } else {
        alert(obj.message);
      }
    } catch (err) {
      console.log(err.message);
    }
  };

  const loadActiveTruck = async () => {
    try {
      const res = await loadActTruck(token);
      const obj = await res.json();
      console.log(obj);
      if (res.status === 200) {
        setActiveTruck(obj.truck);
      } else if (obj.message.toLowerCase().includes('forbidden')) {
        alert('Your session is up. Please log in again');
        logout();
      }
    } catch (err) {
      console.log(err.message);
    }
  };


  const views = trucks.map((t) => {
    return (
      <div key={t._id} className='mb-3'>
        <TruckComponent
          logout={logout}
          token={token}
          assignedTruck={activeTruck}
          reloadData={loadAllData} truck={t}/>
      </div>
    );
  });

  const save = async (event) => {
    event.preventDefault();
    try {
      const res = await saveTruck(token, type);
      const obj = await res.json();
      if (res.status === 200) {
        loadData();
        setType(TruckType.SPRINTER);
      } else if (obj.message.toLowerCase().includes('forbidden')) {
        alert('Your session is up. Please log in again');
        logout();
      } else {
        alert(obj.message);
      }
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <div className='container text-start'>
      <div className='row'>
        <div className='col-lg-8 ms-auto me-auto col-12'>
          <div className='container'>
            <div className='row mt-4 mb-3'>
              <h3 className='col-12'>New Truck</h3>
            </div>
            <div className='row'>
              <form onSubmit={save}>
                <div>
                  <label htmlFor='text'>New truck's type</label>
                  <select
                    value={type}
                    onChange={changeHandler(setType)}
                    id='type'
                    required={true}
                    className='form-control'>
                    <option value={TruckType.SPRINTER}>{TruckType.SPRINTER}</option>
                    <option value={TruckType.SMALL_STRAIGHT}>{TruckType.SMALL_STRAIGHT}</option>
                    <option value={TruckType.LARGE_STRAIGHT}>{TruckType.LARGE_STRAIGHT}</option>
                  </select>
                </div>
                <div>
                  <Button type='submit'>Save</Button>
                </div>
              </form>
            </div>

            <div className='row mt-4 mb-3'>
              <h3 className='col-12 mb-4'>Assigned truck</h3>
              {activeTruck ?
                  <div className='col-12'>
                    <TruckComponent
                        logout={logout}
                        truck={activeTruck}
                        assignedTruck={activeTruck}
                        reloadData={loadAllData}
                        editable={false}
                        token={token}/>
                  </div>
                  :
                  <p>Driver currently has no active truck</p>
              }
            </div>

            <div className='row mt-4 mb-3'>
              <h3 className='col-12'>All Trucks</h3>
            </div>

            {views.length <= 0 &&
                <p className='mt-5 text-center'>You have no trucks</p>}
            <div className='row mb-5'>
              {views}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};


export default TruckListComponent;


