import React, {useEffect, useState} from 'react';
import {useParams} from 'react-router-dom';
import {Button} from 'reactstrap';
import UserRole from "../shared/UserRole";
import LoadStatus from "../shared/LoadStatus";
import {getLoadById, getShippingInfo, updateLoad} from "../services/LoadService";
import {dateToString} from "../services/Utils";

const TOKEN = 'token';

const LoadPageComponent = ({logout, role}) => {
  const params = useParams();

  const [load, setLoad] = useState(null);
  const [truck, setTruck] = useState(null);
  const [message, setMessage] = useState(null);

  const [name, setName] = useState('');
  const [payload, setPayload] = useState('');
  const [pickupAddress, setPickUpAddress] = useState('');
  const [deliveryAddress, setDeliveryAddress] = useState('');
  const [width, setWidth] = useState(0);
  const [length, setLength] = useState(0);
  const [height, setHeight] = useState(0);

  const [editable, setEditable] = useState(false);

  useEffect(() => {
    loadData();
  }, []);

  useEffect(() => {
    fillForm();
  }, [load]);

  const fillForm = () => {
    if (load) {
      setEditable(role === UserRole.SHIPPER && load?.status === LoadStatus.NEW);
      setName(load.name);
      setPayload(load.payload);
      setPickUpAddress(load.pickup_address);
      setDeliveryAddress(load.delivery_address);
      setWidth(load.dimensions.width);
      setLength(load.dimensions.length);
      setHeight(load.dimensions.height);
      loadShippingInfo();
    }
  };

  const loadShippingInfo = async () => {
    if (role === UserRole.SHIPPER && load.status === LoadStatus.ASSIGNED) {
      try {
        const res = await getShippingInfo(params.loadId, token);
        const obj = await res.json();
        if (res.status === 200) {
          setLoad(obj.load);
          setTruck(obj.truck);
        } else if (obj.message.toLowerCase().includes('forbidden')) {
          alert('Your session is up. Please log in again');
          logout();
        } else {
          alert(obj.message);
        }
      } catch (e) {
        console.log(e);
      }
    }
  };

  const token = 'Bearer '+localStorage.getItem(TOKEN);

  const loadData = async () => {
    try {
      const res = await getLoadById(params.loadId, token);
      const obj = await res.json();
      if (res.status === 200) {
        setLoad(obj.load);
      } else if (obj.message.toLowerCase().includes('forbidden')) {
        alert('Your session is up. Please log in again');
        logout();
      } else {
        alert(obj.message);
      }
    } catch (e) {
      console.log(e);
    }
  };

  const changeHandler = (setValue) => (e) => {
    setMessage(null);
    setValue(e.target.value);
  };

  const update = async (event) => {
    setMessage(null);
    event.preventDefault();
    try {
      const res = await updateLoad(params.loadId, token,
          name, payload, pickupAddress, deliveryAddress,
          width, length, height );
      const obj = await res.json();
      if (res.status === 200) {
        loadData();
      } else if (obj.message.toLowerCase().includes('forbidden')) {
        alert('Your session is up. Please log in again');
        logout();
      }
      setMessage(obj.message);
    } catch (e) {
      console.log(e);
    }
  };

  const loadLogs = load?.logs.map((log, index) => {
    return (
        <div key={index} className='ms-3 mb-3'>
          <span>Time: {dateToString(log.time)} </span><br/>
          <span>Message: {log.message}</span>
        </div>
    );
  });

  return (
      <div className='container text-start'>
        <div className='row mb-5'>
          <div className='col-md-8 ms-auto me-auto col-12'>
            <div className='container'>
              <div className='row mt-4 mb-3'>
                <h3 className='col-12'>Load</h3>
              </div>

              {editable &&
                  <div className='row mb-5'>
                    <hr/>
                    <h3 className='col-12 mb-3 mt-4'>Edit Load</h3>
                    <div className='col-12'>
                      <details>
                        <summary>Edit load</summary>
                        <form onSubmit={update}>
                          <div>
                            <label htmlFor='name'>Name</label>
                            <input
                                value={name}
                                type='text'
                                onChange={changeHandler(setName)}
                                id='name'
                                required={true}
                                className='form-control'/>
                          </div>
                          <div>
                            <label htmlFor='payload'>Payload</label>
                            <input
                                type='number'
                                min={0}
                                value={payload}
                                onChange={changeHandler(setPayload)}
                                id='payload'
                                required={true}
                                className='form-control'/>
                          </div>
                          <div>
                            <label htmlFor='pickupAddress'>Pickup address</label>
                            <input
                                type='text'
                                value={pickupAddress}
                                onChange={changeHandler(setPickUpAddress)}
                                id='pickupAddress'
                                required={true}
                                className='form-control'/>
                          </div>
                          <div>
                            <label htmlFor='deliveryAddress'>Delivery address</label>
                            <input
                                type='text'
                                value={deliveryAddress}
                                onChange={changeHandler(setDeliveryAddress)}
                                id='deliveryAddress'
                                required={true}
                                className='form-control'/>
                          </div>
                          <div>
                            <label htmlFor='width'>Width</label>
                            <input
                                type='number'
                                min={0}
                                value={width}
                                onChange={changeHandler(setWidth)}
                                id='width'
                                required={true}
                                className='form-control'/>
                          </div>
                          <div>
                            <label htmlFor='length'>Length</label>
                            <input
                                type='number'
                                min={0}
                                value={length}
                                onChange={changeHandler(setLength)}
                                id='length'
                                required={true}
                                className='form-control'/>
                          </div>
                          <div>
                            <label htmlFor='height'>Height</label>
                            <input
                                type='number'
                                min={0}
                                value={height}
                                onChange={changeHandler(setHeight)}
                                id='height'
                                required={true}
                                className='form-control'/>
                          </div>
                          <div>
                            <Button type='submit'>Update</Button>
                          </div>
                          {message &&
                              <div className='text-center'>
                                <p>{message}</p>
                              </div>
                          }
                        </form>
                      </details>
                    </div>
                  </div>}

              {load &&
                  <div className='row mb-4'>
                    <hr/>
                    <h5>Created by:</h5>
                    <p>{load.created_by}</p>
                    <h5>Assigned to:</h5>
                    <p>{load.assigned_to}</p>
                    <h5>Status:</h5>
                    <p>{load.status}</p>
                    <h5>State:</h5>
                    <p>{load.state}</p>
                    <h5>Created date:</h5>
                    <p>{dateToString(load.created_date)}</p>
                    <h5>Dimensions:</h5>
                    <p>
                      {load.dimensions.width}x
                      {load.dimensions.length}x
                      {load.dimensions.height}
                    </p>
                    <h5>Payload:</h5>
                    <p>{load.payload}</p>
                    <h5>Pickup address:</h5>
                    <p>{load.pickup_address}</p>
                    <h5>Delivery address:</h5>
                    <p>{load.delivery_address}</p>
                    <h5>Logs</h5>
                    <details>
                      <summary>Details</summary>
                      {loadLogs}
                    </details>
                  </div>}

              {truck &&
                  <div className='row mb-4'>
                    <hr/>
                    <h3>Truck</h3>
                    <details>
                      <summary>Details</summary>
                      <h5 className='pt-3'>Created by:</h5>
                      <p>{truck.created_by}</p>
                      <h5>Assigned to:</h5>
                      <p>{truck.assigned_to}</p>
                      <h5>Status:</h5>
                      <p>{truck.status}</p>
                      <h5>Created date:</h5>
                      <p>{dateToString(truck.created_date)}</p>
                      <h5>Type:</h5>
                      <p>{truck.type}</p>
                    </details>
                  </div>}
            </div>
          </div>
        </div>
      </div>
  );
};


export default LoadPageComponent;


