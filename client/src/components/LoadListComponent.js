import React, {useEffect, useState} from 'react';
import {Button} from 'reactstrap';
import {useNavigate} from 'react-router-dom';
import UserRole from "../shared/UserRole";
import {deleteLoad, loadActLoad, loadLoads, nextActiveLoadState, postLoad, saveLoad} from "../services/LoadService";
import LoadStatus from "../shared/LoadStatus";

const TOKEN = 'token';

const LoadComponent = ({load, role, reloadData, token, logout}) => {

  const navigate = useNavigate();

  const canBePosted = role === UserRole.SHIPPER && load.status === LoadStatus.NEW;
  const canBeDeleted = canBePosted;
  const canBeUpdated = canBePosted;
  const canBeIterated = role === UserRole.DRIVER && load.status !== LoadStatus.SHIPPED;

  const post = async () => {
    try {
      const res = await postLoad(load._id, token);
      const obj = await res.json();
      console.log('post load res object: ', obj);
      if (res.status === 200) {
        if (!obj.driver_found) {
          alert('Driver not found. Load not posted');
        } else {
          reloadData();
        }
      } else if (obj.message.toLowerCase().includes('forbidden')) {
        alert('Your session is up. Please log in again');
        logout();
      } else {
        alert(obj.message);
      }
    } catch (e) {
      console.log(e);
    }
  };

  const delLoad = async () => {
    try {
      const res = await deleteLoad(load._id, token);
      const obj = await res.json();
      if (res.status === 200) {
        reloadData();
      } else if (obj.message.toLowerCase().includes('forbidden')) {
        alert('Your session is up. Please log in again');
        logout();
      } else {
        alert(obj.message);
      }
    } catch (e) {
      console.log(e);
    }
  };

  const goToLoadPage = () => {
    // let path;
    // if (canBeUpdated) {
    //   path = `${load._id}/edit`;
    // } else {
    //   path = `${load._id}/details`
    // }
    navigate(`${load._id}`, {replace: false});
  };

  const nextState = async () => {
    try {
      const res = await nextActiveLoadState(token);
      const obj = await res.json();
      if (res.status === 200) {
        reloadData();
      } else if (obj.message.toLowerCase().includes('forbidden')) {
        alert('Your session is up. Please log in again');
        logout();
      } else {
        alert(obj.message);
      }
    } catch (e) {
      console.log(e);
    }
  }

  return (
      <div
          className='note-card flex-wrap flex-md-row flex-column gap-2
        d-flex justify-content-between align-items-center'>
        <div>
          {load.name} | {load.status} {load.state ? `| ${load.state} | `: ''}
          {load.dimensions.width}x
          {load.dimensions.length}x
          {load.dimensions.height}, {load.payload}
        </div>
        <div className='d-flex gap-3 flex-wrap justify-content-center'>
          <Button onClick={goToLoadPage}>{canBeUpdated ? 'Edit' : 'Details'}</Button>

          {canBePosted &&
              <Button onClick={post} className='btn-info'>Post</Button>}

          {canBeIterated &&
              <Button onClick={nextState} className='btn-info'>State ></Button>}

          {canBeDeleted &&
              <Button onClick={delLoad} className='btn-danger'>Delete</Button>}

        </div>
      </div>
  );
};


const LoadListComponent = ({logout, role}) => {

  const statuses = role === UserRole.DRIVER ?
      [LoadStatus.SHIPPED, LoadStatus.ASSIGNED]
      : [LoadStatus.NEW, LoadStatus.POSTED, LoadStatus.SHIPPED, LoadStatus.ASSIGNED];

  const [loads, setLoads] = useState([]);
  const [activeLoad, setActiveLoad] = useState(null);

  const [offset, setOffset] = useState(0);
  const [limit, setLimit] = useState(0);
  const [status, setStatus] = useState('');

  const [name, setName] = useState('');
  const [payload, setPayload] = useState('');
  const [pickupAddress, setPickUpAddress] = useState('');
  const [deliveryAddress, setDeliveryAddress] = useState('');
  const [width, setWidth] = useState(0);
  const [length, setLength] = useState(0);
  const [height, setHeight] = useState(0);

  const token = 'Bearer '+localStorage.getItem(TOKEN);

  useEffect(() => {
    loadAllData();
  }, []);

  useEffect(() => {
    loadData();
  }, [status, offset, limit]);

  const changeHandler = (setValue) => (e) => {
    setValue(e.target.value);
  };

  const loadAllData = () => {
    console.log('role: '+role);
    loadData();
    if (role === UserRole.DRIVER) {
      loadActiveLoad();
    }
  }

  const loadData = async () => {
    try {
      const res = await loadLoads(token, status, offset, limit);
      const obj = await res.json();
      console.log('loads loaded: ',obj);
      if (res.status === 200) {
        setLoads(obj.loads);
      } else if (obj.message.toLowerCase().includes('forbidden')) {
        alert('Your session is up. Please log in again');
        logout();
      } else {
        alert(obj.message);
      }
    } catch (err) {
      console.log(err.message);
    }
  };

  const loadActiveLoad = async () => {
    try {
      const res = await loadActLoad(token);
      const obj = await res.json();
      console.log('driver active load', obj);
      if (res.status === 200) {
        setActiveLoad(obj.load);
      } else if (obj.message.toLowerCase().includes('forbidden')) {
        alert('Your session is up. Please log in again');
        logout();
      } else {
        setActiveLoad(null);
      }
    } catch (err) {
      console.log(err.message);
    }
  };


  const views = loads.map((l) => {
    return (
        <div key={l._id} className='mb-3'>
          <LoadComponent
              role={role}
              logout={logout}
              token={token}
              reloadData={loadAllData}
              load={l}/>
        </div>
    );
  });

  const resetForm = () => {
    setName('');
    setPayload(0);
    setPickUpAddress('');
    setDeliveryAddress('');
    setWidth(0);
    setLength(0);
    setHeight(0);
  }

  const save = async (event) => {
    event.preventDefault();
    try {
      const res = await saveLoad(token, name, payload,
          pickupAddress, deliveryAddress, width, length, height);
      const obj = await res.json();
      if (res.status === 200) {
        loadData();
        resetForm();
      } else if (obj.message.toLowerCase().includes('forbidden')) {
        alert('Your session is up. Please log in again');
        logout();
      } else {
        alert(obj.message);
      }
    } catch (err) {
      console.log(err);
    }
  };

  const loadStatusOptions = statuses.map(s => {
    return (
        <option key={s} value={s}>{s}</option>
    );
  });

  return (
      <div className='container text-start'>
        <div className='row'>
          <div className='col-lg-8 ms-auto me-auto col-12'>
            <div className='container'>
              {role === UserRole.SHIPPER &&
                  <>
                    <div className='row mt-4 mb-3'>
                      <h3 className='col-12'>New Load</h3>
                    </div>
                    <div className='row'>
                      <div className='col-12'>
                        <details>
                          <summary>Add new load</summary>
                          <form onSubmit={save}>
                            <div>
                              <label htmlFor='name'>Name</label>
                              <input
                                  value={name}
                                  type='text'
                                  onChange={changeHandler(setName)}
                                  id='name'
                                  required={true}
                                  className='form-control'/>
                            </div>
                            <div>
                              <label htmlFor='payload'>Payload</label>
                              <input
                                  type='number'
                                  min={0}
                                  value={payload}
                                  onChange={changeHandler(setPayload)}
                                  id='payload'
                                  required={true}
                                  className='form-control'/>
                            </div>
                            <div>
                              <label htmlFor='pickupAddress'>Pickup address</label>
                              <input
                                  type='text'
                                  value={pickupAddress}
                                  onChange={changeHandler(setPickUpAddress)}
                                  id='pickupAddress'
                                  required={true}
                                  className='form-control'/>
                            </div>
                            <div>
                              <label htmlFor='deliveryAddress'>Delivery address</label>
                              <input
                                  type='text'
                                  value={deliveryAddress}
                                  onChange={changeHandler(setDeliveryAddress)}
                                  id='deliveryAddress'
                                  required={true}
                                  className='form-control'/>
                            </div>
                            <div>
                              <label htmlFor='width'>Width</label>
                              <input
                                  type='number'
                                  min={0}
                                  value={width}
                                  onChange={changeHandler(setWidth)}
                                  id='width'
                                  required={true}
                                  className='form-control'/>
                            </div>
                            <div>
                              <label htmlFor='length'>Length</label>
                              <input
                                  type='number'
                                  min={0}
                                  value={length}
                                  onChange={changeHandler(setLength)}
                                  id='length'
                                  required={true}
                                  className='form-control'/>
                            </div>
                            <div>
                              <label htmlFor='height'>Height</label>
                              <input
                                  type='number'
                                  min={0}
                                  value={height}
                                  onChange={changeHandler(setHeight)}
                                  id='height'
                                  required={true}
                                  className='form-control'/>
                            </div>
                            <div>
                              <Button type='submit'>Save</Button>
                            </div>
                          </form>
                        </details>
                      </div>
                    </div>
                  </>
              }

              {role === UserRole.DRIVER &&
                  <div className='row mt-4 mb-3'>
                    <h3 className='col-12 mb-4'>Active load</h3>
                    {activeLoad ?
                        <div className='col-12'>
                          <LoadComponent
                              logout={logout}
                              load={activeLoad}
                              reloadData={loadAllData}
                              role={role}
                              token={token}/>
                        </div>
                        :
                        <p>Driver currently has no active load</p>
                    }
                  </div>
              }

              <div className='row mt-4 mb-3'>
                <h3 className='col-12'>All Loads</h3>
              </div>


              <div className='row mb-4'>
                <div className='col-12 d-flex flex-wrap
                gap-4 align-items-center'>
                  <div>
                    <label htmlFor='offset'>offset:</label>
                    <input id='offset'
                           onChange={changeHandler(setOffset)}
                           value={offset}
                           type='number'
                           className='ms-3 form-control
                    d-inline-block inp-param' min={0}/>
                  </div>
                  <div>
                    <label htmlFor='limit'>limit:</label>
                    <input id='limit'
                           onChange={changeHandler(setLimit)}
                           value={limit}
                           className='ms-3 form-control
                    d-inline-block inp-param'
                           type='number' min={0}/>
                  </div>
                  <div>
                    <label htmlFor='status'>status:</label>
                    <select id='offset'
                           onChange={changeHandler(setStatus)}
                           value={status}
                           className='ms-3 form-control
                    d-inline-block inp-param'>
                      <option value=''>ANY</option>
                      {loadStatusOptions}
                    </select>
                  </div>
                </div>
              </div>


              {views.length <= 0 &&
                  <p className='mt-5 text-center'>No loads</p>}
              <div className='row mb-5'>
                {views}
              </div>
            </div>
          </div>
        </div>
      </div>
  );
};


export default LoadListComponent;


