import React, {useState} from 'react';
import {login, register, resetPassword} from '../services/AuthService';
import {Button} from 'reactstrap';
import '../styles/login.css';
import UserRole from "../shared/UserRole";

const TOKEN = 'token';

const LoginComponent = ({setUserLoggedIn}) => {
  const [mode, setMode] = useState('login');

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [message, setMessage] = useState(null);
  const [registerRole, setRegisterRole] = useState(UserRole.DRIVER);

  const saveToken = (token) => {
    localStorage.setItem(TOKEN, token);
  };

  const loginUser = async (event) => {
    event.preventDefault();
    try {
      const res = await login(email, password);
      const obj = await res.json();
      if (res.status === 200) {
        saveToken(obj.jwt_token);
        setUserLoggedIn(true);
      } else {
        setMessage(obj.message);
      }
    } catch (err) {
      alert(err);
    }
  };

  const sendPasswordEmail = async (event) => {
    console.log('here');
    event.preventDefault();
    try {
      const res = await resetPassword(email);
      const obj = await res.json();
      setMessage(obj.message);
    } catch (err) {
      alert(err);
    }
  };

  const registerUser = async (event) => {
    event.preventDefault();
    try {
      const res = await register(email, password, registerRole);
      const obj = await res.json();
      setMessage(obj.message);
    } catch (err) {
      alert(err);
    }
  };

  const switchMode = (mode1, mode2) => {
    if (mode === mode1) {
      setMode(mode2);
    } else {
      setMode(mode1);
    }
    setMessage(null);
  };

  const changeHandler = (setValue) => (e) => {
    setMessage(null);
    setValue(e.target.value);
  };

  return (
    <div className='container text-start'>
      <div className='row mt-5'>
        <form className='col-md-6 col-lg-4 col-8 ms-auto me-auto'
          onSubmit={mode === 'login' ? loginUser: mode === 'signup' ? registerUser : sendPasswordEmail}>
          <div>
            <h3 className='text-center'>
              {mode === 'login'?'Sign In': mode === 'signup' ? 'Sign Up' : 'Forgot password'}
            </h3>
          </div>
          {message &&
                        <div className='text-center'>
                          <p>{message}</p>
                        </div>}
          <div>
            <label htmlFor='email'>Email</label>
            <input
              value={email}
              onChange={changeHandler(setEmail)}
              type='email' id='email'
              required={true} className='form-control'/>
          </div>
          {mode !== 'forgot' &&
              <div>
                <label htmlFor='password'>Password</label>
                <input
                  value={password}
                  onChange={changeHandler(setPassword)}
                  type='password' id='password'
                  required={true} className='form-control'/>
              </div>}
          {mode === 'signup' &&
              <div>
                <label htmlFor='registerRole'>Role</label>
                <select
                    value={registerRole}
                    onChange={changeHandler(setRegisterRole)}
                    id='registerRole'
                    required={true} className='form-control'>
                  <option value={UserRole.DRIVER}>{UserRole.DRIVER}</option>
                  <option value={UserRole.SHIPPER}>{UserRole.SHIPPER}</option>
                </select>
              </div>}
          <div>
            <Button className='mb-2' type='submit'>Submit</Button>
            {mode !== 'forgot' &&
                <Button
                  type='button'
                  onClick={() => switchMode('login', 'signup')}>{mode === 'login' ? 'Sign Up' : 'Sign In'}
                </Button>}
            <Button
                onClick={() => switchMode('login', 'forgot')}
                className='mt-2' type='button'>{mode === 'forgot' ? 'Sign In': 'Forgot password'}</Button>
          </div>
        </form>
      </div>

    </div>
  );
};

export default LoginComponent;
