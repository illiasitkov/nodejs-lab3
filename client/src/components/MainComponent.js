import React, {useEffect, useState} from 'react';
import LoginComponent from './LoginComponent';
import {Routes, Route, Navigate} from 'react-router-dom';
import TruckListComponent from './TruckListComponent';
import HeaderComponent from './HeaderComponent';
import ProfileComponent from './ProfileComponent';
import UserRole from "../shared/UserRole";
import TruckPageComponent from "./TruckPageComponent";
import {getUserRole} from "../services/AuthService";
import LoadListComponent from "./LoadListComponent";
import LoadPageComponent from "./LoadPageComponent";

const TOKEN = 'token';

const MainComponent = () => {
  const [userLoggedIn, setUserLoggedIn] = useState(null);
  const [role, setRole] = useState(null);

  useEffect(() => {
    checkUserLoggedIn();
  }, []);

  useEffect(() => {
    changeUserRole();
  }, [userLoggedIn]);

  const changeUserRole = async () => {
    if (userLoggedIn) {
      const token = localStorage.getItem(TOKEN);
      const bearer = 'Bearer ' + token;
      const res1 = await getUserRole(bearer);
      const obj1 = await res1.json();
      console.log(obj1);
      setRole(obj1.role);
    } else {
      setRole(null);
    }
  };

  const checkUserLoggedIn = () => {
    if (localStorage.getItem(TOKEN)) {
      setUserLoggedIn(true);
    } else {
      setUserLoggedIn(false);
    }
  };

  const logout = () => {
    localStorage.removeItem(TOKEN);
    setUserLoggedIn(false);
  };


  return (
      <>
        {userLoggedIn !== null && !userLoggedIn &&
            <Routes>
              <Route
                  path='/login' exact
                  element=
                      {<LoginComponent setRole={setRole} setUserLoggedIn={setUserLoggedIn}/>}/>
              <Route path='*' element={<Navigate to='/login'/>}/>
            </Routes>
        }
        {userLoggedIn !== null && userLoggedIn && role !== null &&
            <>
              <HeaderComponent role={role} logout={logout}/>
              <Routes>
                <Route
                    path='/loads'
                    exact element={<LoadListComponent role={role} logout={logout}/>}/>
                <Route
                    path='/loads/:loadId'
                    exact element={<LoadPageComponent role={role} logout={logout}/>}/>
                <Route
                    path='/profile'
                    exact element={<ProfileComponent role={role} logout={logout}/>}/>
                {role === UserRole.DRIVER &&
                    <Route
                        path='/trucks'
                        exact element={<TruckListComponent logout={logout}/>}/>}
                {role === UserRole.DRIVER &&
                    <Route
                        path='/trucks/:truckId/edit'
                        element={<TruckPageComponent editable={true} logout={logout}/>}/>}
                {role === UserRole.DRIVER &&
                    <Route
                        path='/trucks/:truckId/details'
                        element={<TruckPageComponent editable={false} logout={logout}/>}/>}
                <Route path='*' element={<Navigate to={role === UserRole.DRIVER ? '/trucks' : '/loads'}/>}/>
              </Routes>
            </>
        }
      </>
  );
};


export default MainComponent;

