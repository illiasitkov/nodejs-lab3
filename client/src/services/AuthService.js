
const url = 'http://localhost:8080/api/auth';

const login = (email, password) => {
  return fetch(`${url}/login`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({email, password}),
  });
};

const register = (email, password, role) => {
  return fetch(`${url}/register`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({email, password, role}),
  });
};

const resetPassword = (email) => {
  return fetch(`${url}/forgot_password`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({email}),
  });
}

const getUserRole = (token) => {
  return fetch(`${url}/role`, {
    method: 'GET',
    headers: {
      'Authorization': token,
    },
  });
}

exports.login = login;
exports.register = register;
exports.resetPassword = resetPassword;
exports.getUserRole = getUserRole;
