
const url = 'http://localhost:8080/api/loads';


const loadLoads = (token, status, offset, limit) => {
  return fetch(`${url}?status=${status}&offset=${offset}&limit=${limit}`, {
    method: 'GET',
    headers: {
      'Authorization': token,
    },
  });
};

const loadActLoad = (token) => {
  return fetch(`${url}/active`, {
    method: 'GET',
    headers: {
      'Authorization': token,
    },
  });
};

const nextActiveLoadState = (token) => {
  return fetch(`${url}/active/state`, {
    method: 'PATCH',
    headers: {
      'Authorization': token,
    },
  });
};

const deleteLoad = (loadId, token) => {
  return fetch(`${url}/${loadId}`, {
    method: 'DELETE',
    headers: {
      'Authorization': token,
    },
  });
};


const postLoad = (loadId, token) => {
  return fetch(`${url}/${loadId}/post`, {
    method: 'POST',
    headers: {
      'Authorization': token,
    },
  });
};

const saveLoad = (token, name, payload,
                  pickup_address, delivery_address, width, length, height) => {
  return fetch(`${url}`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': token,
    },
    body: JSON.stringify({
      name, payload, pickup_address, delivery_address,
      dimensions: {
        width, length, height,
      },
    }),
  });
};

const getLoadById = (loadId, token) => {
  return fetch(`${url}/${loadId}`, {
    method: 'GET',
    headers: {
      'Authorization': token,
    },
  });
};

const updateLoad = (truckId, token, name, payload,
                    pickup_address, delivery_address,
                    width, length, height) => {
  return fetch(`${url}/${truckId}`, {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': token,
    },
    body: JSON.stringify({
      name, payload, pickup_address, delivery_address,
      dimensions: {
        width, length, height,
      }}
    ),
  });
};

const getShippingInfo = (loadId, token) => {
  return fetch(`${url}/${loadId}/shipping_info`, {
    method: 'GET',
    headers: {
      'Authorization': token,
    },
  });
}


exports.loadLoads = loadLoads;
exports.nextActiveLoadState = nextActiveLoadState;
exports.deleteLoad = deleteLoad;
exports.saveLoad = saveLoad;
exports.getLoadById = getLoadById;
exports.updateLoad = updateLoad;
exports.loadActLoad = loadActLoad;
exports.postLoad = postLoad;
exports.getShippingInfo = getShippingInfo;

