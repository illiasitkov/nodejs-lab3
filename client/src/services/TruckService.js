
const url = 'http://localhost:8080/api/trucks';


const loadTrucks = (token) => {
  return fetch(`${url}`, {
    method: 'GET',
    headers: {
      'Authorization': token,
    },
  });
};

const loadActTruck = (token) => {
  return fetch(`${url}/assigned`, {
    method: 'GET',
    headers: {
      'Authorization': token,
    },
  });
};

const checkNote = (noteId, token) => {
  return fetch(`${url}/${noteId}`, {
    method: 'PATCH',
    headers: {
      'Authorization': token,
    },
  });
};

const deleteTruck = (truckId, token) => {
  return fetch(`${url}/${truckId}`, {
    method: 'DELETE',
    headers: {
      'Authorization': token,
    },
  });
};


const assignTruck = (truckId, token) => {
  return fetch(`${url}/${truckId}/assign`, {
    method: 'POST',
    headers: {
      'Authorization': token,
    },
  });
};

const saveTruck = (token, type) => {
  return fetch(`${url}`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': token,
    },
    body: JSON.stringify({type}),
  });
};

const getTruckById = (truckId, token) => {
  return fetch(`${url}/${truckId}`, {
    method: 'GET',
    headers: {
      'Authorization': token,
    },
  });
};

const updateTruck = (truckId, type, token) => {
  return fetch(`${url}/${truckId}`, {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': token,
    },
    body: JSON.stringify({type}),
  });
};

exports.loadTrucks = loadTrucks;
exports.checkNote = checkNote;
exports.deleteTruck = deleteTruck;
exports.saveTruck = saveTruck;
exports.getTruckById = getTruckById;
exports.updateTruck = updateTruck;
exports.loadActTruck = loadActTruck;
exports.assignTruck = assignTruck;
