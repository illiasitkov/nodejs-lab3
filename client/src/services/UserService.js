
const url = 'http://localhost:8080/api/users/me';


const changePassword = (token, oldPassword, newPassword) => {
  return fetch(`${url}/password`, {
    method: 'PATCH',
    headers: {
      'Content-type': 'application/json',
      'Authorization': token,
    },
    body: JSON.stringify({oldPassword, newPassword}),
  });
};

const getUserProfile = (token) => {
  return fetch(`${url}`, {
    method: 'GET',
    headers: {
      'Authorization': token,
    },
  });
};

const deleteUser = (token) => {
  return fetch(`${url}`, {
    method: 'DELETE',
    headers: {
      'Authorization': token,
    },
  });
};


exports.changePassword = changePassword;
exports.getUserProfile = getUserProfile;
exports.deleteUser = deleteUser;


