
const dateToString = (dateString) => {
    const date = new Date(dateString);
    return date.toLocaleDateString() +' ' + date.toLocaleTimeString();
};


exports.dateToString = dateToString;

