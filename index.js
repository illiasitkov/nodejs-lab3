const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');

const dotenv = require('dotenv');
dotenv.config();


const {fileLogger, consoleLogger} = require('./utils/logging');

const errorHandler = require('./routers/errorHandler');
const authRouter = require('./routers/authRouter');
const usersRouter = require('./routers/usersRouter');
const trucksRouter = require('./routers/trucksRouter');
const UserRole = require('./utils/UserRole');
const loadsRouter = require('./routers/loadsRouter');
const {authMiddlewareWithRole} = require('./routers/authMiddleware');

const dbUrl = process.env.MONGODB_URL;

const server = express();

server.use(cors());
server.use(fileLogger);
server.use(consoleLogger);
server.use(express.json());

server.use('/api/users/me', authMiddlewareWithRole(UserRole.ANY), usersRouter);
server.use('/api/trucks',
    authMiddlewareWithRole(UserRole.DRIVER), trucksRouter);
server.use('/api/loads',
    authMiddlewareWithRole(UserRole.ANY), loadsRouter);
server.use('/api/auth', authRouter);

server.use(errorHandler);


mongoose.connect(dbUrl)
    .then((_) => {
      console.log('Connected to db server');
    })
    .catch((err) => {
      console.log(err);
    });


server.listen(process.env.PORT);


