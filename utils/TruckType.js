
const TruckType = {
  SPRINTER: {
    name: 'SPRINTER',
    dimensions: {
      width: 300,
      length: 250,
      height: 170,
    },
    max_weight: 1700,
  },
  SMALL_STRAIGHT: {
    name: 'SMALL STRAIGHT',
    dimensions: {
      width: 500,
      length: 250,
      height: 170,
    },
    max_weight: 2500,
  },
  LARGE_STRAIGHT: {
    name: 'LARGE STRAIGHT',
    dimensions: {
      width: 700,
      length: 350,
      height: 200,
    },
    max_weight: 4000,
  },
};

module.exports = TruckType;

