const morgan = require('morgan');
const path = require('path');
const fs = require('fs');

const dir = path.join(__dirname, path.normalize('../logs'));
const logFileName = 'app.log';

const fileLogger = ()=> {
  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir);
  }
  const logStream =
      fs.createWriteStream(path.join(dir, logFileName), {flags: 'a+'});
  return morgan('combined', {stream: logStream});
};

exports.fileLogger = fileLogger();
exports.consoleLogger = morgan('dev');
