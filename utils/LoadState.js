
const nextState = (state) => {
  switch (state) {
    case LoadState.STATE_0:
      return LoadState.STATE_1;
    case LoadState.STATE_1:
      return LoadState.STATE_2;
    case LoadState.STATE_2:
      return LoadState.STATE_3;
    default:
      return LoadState.STATE_3;
  }
};

const LoadState = {
  STATE_0: 'En route to Pick Up',
  STATE_1: 'Arrived to Pick Up',
  STATE_2: 'En route to delivery',
  STATE_3: 'Arrived to delivery',
  nextState,
};

module.exports = LoadState;


