const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const userSchema = new Schema({
  email: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    min: 4,
    required: true,
  },
  role: {
    type: String,
    required: true,
  },
}, {timestamps: true, strict: true});


const User = mongoose.model('User', userSchema);

module.exports = User;
