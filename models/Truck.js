
const mongoose = require('mongoose');

const Schema = mongoose.Schema;


const truckSchema = new Schema({
  created_by: {
    type: String,
    required: true,
  },
  assigned_to: {
    type: String,
    required: false,
  },
  type: {
    type: String,
    required: true,
  },
  status: {
    type: String,
    required: true,
  },
}, {timestamps: true, strict: true});


const Truck = mongoose.model('Truck', truckSchema);

module.exports = Truck;
