
const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const logSchema = new Schema({
  message: {
    type: String,
    required: true,
  },
  time: {
    type: Date,
    required: true,
  },
}, {_id: false});


const dimensionsSchema = new Schema({
  width: {
    type: Number,
    required: true,
  },
  length: {
    type: Number,
    required: true,
  },
  height: {
    type: Number,
    required: true,
  },
}, {_id: false});


const loadSchema = new Schema({
  created_by: {
    type: String,
    required: true,
  },
  assigned_to: {
    type: String,
    required: false,
  },
  status: {
    type: String,
    required: false,
  },
  state: {
    type: String,
    required: false,
  },
  name: {
    type: String,
    required: true,
  },
  payload: {
    type: Number,
    required: true,
  },
  pickup_address: {
    type: String,
    required: true,
  },
  delivery_address: {
    type: String,
    required: true,
  },
  dimensions: {
    type: dimensionsSchema,
    required: true,
  },
  logs: [logSchema],
}, {timestamps: true, strict: true});


const Load = mongoose.model('Load', loadSchema);


module.exports = Load;


