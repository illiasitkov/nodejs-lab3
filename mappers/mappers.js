
const dbUserToUserGetDto = (dbUser) => ({
  _id: dbUser._id,
  email: dbUser.email,
  role: dbUser.role,
  created_date: dbUser.createdAt,
});


const dbTruckToTruckGetDto = (dbTruck) => ({
  _id: dbTruck._id,
  status: dbTruck.status,
  created_by: dbTruck.created_by,
  assigned_to: dbTruck.assigned_to,
  type: dbTruck.type,
  created_date: dbTruck.createdAt,
});

const dbTrucksToTrucksGetDto = (trucks) => {
  return trucks.map((t) => dbTruckToTruckGetDto(t));
};

const dbLoadToLoadGetDto = (dbLoad) => ({
  _id: dbLoad._id,
  created_by: dbLoad.created_by,
  assigned_to: dbLoad.assigned_to,
  status: dbLoad.status,
  state: dbLoad.state,
  name: dbLoad.name,
  payload: dbLoad.payload,
  pickup_address: dbLoad.pickup_address,
  delivery_address: dbLoad.delivery_address,
  dimensions: dbLoad.dimensions,
  logs: dbLoad.logs,
  created_date: dbLoad.createdAt,
});

const dbLoadsToLoadsGetDto = (loads) => {
  return loads.map((t) => dbLoadToLoadGetDto(t));
};


exports.dbUserToUserGetDto = dbUserToUserGetDto;
exports.dbTrucksToTrucksGetDto = dbTrucksToTrucksGetDto;
exports.dbTruckToTruckGetDto = dbTruckToTruckGetDto;
exports.dbLoadsToLoadsGetDto = dbLoadsToLoadsGetDto;
exports.dbLoadToLoadGetDto = dbLoadToLoadGetDto;
